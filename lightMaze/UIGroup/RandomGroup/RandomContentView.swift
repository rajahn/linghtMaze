//
//  ContentView.swift
//  lightMaze
//
//  Created by aby on 2019/11/15.
//  Copyright © 2019 aby. All rights reserved.
//

import SwiftUI

struct RandomContentView: View {
    
    let viewModel: RandomViewModel
    init(viewModel: RandomViewModel) {
        self.viewModel = viewModel
    }
    
    @ObservedObject var gameManager = GameManager.init(gameType: .random)
    
    @State var isShowHistory = false
    
    var body: some View {
        VStack {
            HStack {
                button(title: "开始", color: .clear, size: CGSize.init(width: 40, height: 40)) {
                    self.gameManager.startRandomPlay()
                }
                Spacer()
                button(title: "提示", color: .clear, size: CGSize.init(width: 40, height: 40)) {
                    
                }
            }
            
            HStack {
                Text("\(gameManager.timeString)")
                    .font(.system(size: 45))
                    .foregroundColor(Color.black)
                Spacer()
                Text("\(gameManager.clickTimes)步")
                    .font(.system(size: 44))
                    .foregroundColor(Color.black)
                
            }.padding(20)
            
            ForEach(0..<gameManager.lights.count) { row in
                HStack(spacing: 20) {
                    ForEach(0..<self.gameManager.lights[row].count) { column in
                        Circle()
                            .foregroundColor(self.gameManager.lights[row][column].status ? .yellow : .gray)
                            .opacity(self.gameManager.lights[row][column].status ? 0.8 : 0.5)
                            .frame(width: self.gameManager.circleWidth(),
                                   height: self.gameManager.circleWidth())
                            .shadow(color: .yellow, radius: self.gameManager.lights[row][column].status ? 10 : 0)
                            .onTapGesture {
                                self.gameManager.clickTimes += 1
                                self.gameManager.updateLightStatus(column: column, row: row)
                        }
                    }
                }
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0))
            }.padding(20)
            
            HStack {
                Text("\(gameManager.answerString)")
                .font(.system(size: 20))
                .foregroundColor(Color.clear)
            }.padding(20)
            
        }
        .alert(isPresented: $gameManager.isWin) {
            Alert(title: Text("黑灯瞎火，摸鱼成功！"), dismissButton: .default(Text("继续摸鱼"), action: {
                self.gameManager.loadRandomData()
                self.gameManager.startRandomPlay()
                self.gameManager.timerRestart()
            }))
        }
    }
    
    func button(title: String, color: Color, size: CGSize, action:@escaping () -> Void) -> some View {
        return ZStack {
            Rectangle()
                .frame(width: size.width, height: size.height, alignment: .center)
                .foregroundColor(color)
                .cornerRadius(15)
            Text(title)
                .bold()
        }.onTapGesture {
            action()
        }
    }
}

struct RandomContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            RandomContentView(viewModel: RandomViewModel.init())
                .environment(\.colorScheme, .light)
            
            RandomContentView(viewModel: RandomViewModel.init())
                .environment(\.colorScheme, .dark)
        }
    }
}
