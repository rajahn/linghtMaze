//
//  AppdependenceContainer.swift
//  lightMaze
//
//  Created by aby on 2019/11/15.
//  Copyright © 2019 aby. All rights reserved.
//

import UIKit

class AppdependencyContainer {
    
    let shareNavViewModel: MainNavViewModel
    init() {
        self.shareNavViewModel = MainNavViewModel.init()
    }
    
    func makeMainNavController() -> UIViewController {
        return MainNaviagtaonController.init(viewModel: shareNavViewModel,
                                             randomViewControllerFactory: self.makeRandomGroupViewController,
                                             firstViewControllerFactory: self.makeFirstViewController,
                                             missionViewModelFactory: self.makeMissionViewController)
    }
    
    func makeFirstViewController() -> UIViewController {
        let viewController = SetpViewController.init(viewModelFactory: self)
        return viewController
    }
    
    func makeRandomGroupViewController() -> UIViewController {
        let viewController  = RandomViewController.init(viewModelFactory: self)
        return viewController
    }
    
    func makeMissionViewController() -> UIViewController {
        let viewController = MissionViewController.init(viewModelFactory: self)
        return viewController
    }
}

extension AppdependencyContainer: RandomViewModelFactory {
    func makeRandomViewModel() -> RandomViewModel {
        return RandomViewModel.init(pop: shareNavViewModel)
    }
}

extension AppdependencyContainer: SetpViewModelFactory {
    func makeSetpViewModel() -> SetpViewModel {
        return SetpViewModel.init(toRandom: shareNavViewModel, missonResponder: shareNavViewModel)
    }
}

extension AppdependencyContainer: MissionViewModelFactory {
    func makeMissionViewModel() -> MissionViewModel {
        return MissionViewModel.init(pop: shareNavViewModel, size: 5, lightSquene: [5,8,9,2,7])
    }
}
